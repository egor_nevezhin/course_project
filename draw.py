# coding: utf-8
# Python2.7

from lib.vertex import Vertex
from lib.edge import Edge
from lib.graph import Graph
from lib.io import from_vk_file_to_adj
import pygraphviz as pgv

# Adj list
adj = from_vk_file_to_adj('vk/3240_egor_nevezhin.txt')
print adj
g = pgv.AGraph(adj)
g.draw('egors.svg', prog='dot') 
