# coding: utf-8
from .vertex import Vertex
from .edge import Edge

class Graph:
    def __init__(self):
        self.__vertecies = []
        self.__edges = []
        self.__diameter = None

    def __str__(self):
        head = '\n\n\n  Graph\n\n'
        vertecies = 'Verts: \n{} \n\n'.format(''.join([str(vert) for vert in self.get_vertecies()]))
        edges = 'Edges: \n{}'.format(''.join(['{}—{}\n'.format(edge.get_beg(), edge.get_end()) for edge in self.get_edges()]))
        return head + vertecies + edges

    def get_diameter(self):
        return self.__diameter

    def set_diameter(self, diameter):
        self.__diameter = diameter

    def get_edges(self):
        return self.__edges

    def get_vertecies(self):
        return self.__vertecies

    def get_degrees(self):
        dict = {}
        for edge in self.__edges:
            beg = edge.get_beg().get_value()
            if beg in dict:
                dict[beg] += 1
            else:
                dict[beg] = 1
        return dict

    def get_degree_distribution(self):
        degrees = self.get_degrees()
        values = list(degrees.values())
        res = {}
        for degree in values:
            if not degree in res:
                res[degree] = values.count(degree)
        return res

    def get_vertex_by_degree(self, degree):
        degrees = self.get_degrees()
        matched = []
        for id, deg in degrees.items():
            if (deg == degree):
                if self.get_vertex(id):
                    matched.append(self.get_vertex(id))
        return matched

    def get_vertex(self, id):
        for vertex in self.__vertecies:
            if (vertex.get_value() == id):
                return vertex
        return None

    def get_edges_amount(self):
        return len(self.__edges)

    def get_vertecies_amount(self):
        return len(self.__vertecies)

    def insert_vertex(self, vertex):
        if isinstance(vertex, Vertex):
            self.__vertecies.append(vertex)
        elif isinstance(vertex, int):
            vertex = Vertex(vertex)
            self.__vertecies.append(vertex)

    def insert_edge(self, v1, v2 = None):
        if isinstance(v1, Edge):
            self.__edges.append(v1)
        elif isinstance(v1, Vertex) and isinstance(v2, Vertex):
            if self.get_vertex(v1.get_value()) and self.get_vertex(v2.get_value()):
                edge = Edge(v1, v2)
                self.__edges.append(edge)
        elif isinstance(v1, int) and isinstance(v2, int):
            if self.get_vertex(v1) and self.get_vertex(v2):
                edge = Edge(v1, v2)
                self.__edges.append(edge)

    def find(self, vertex):
        for i, vert in enumerate(self.__vertecies):
            if isinstance(vertex, Vertex):
                if vertex == vert:
                    return i
            elif isinstance(vertex, int):
                if vertex == vert.get_value():
                    return i
        return -1


    def remove(self, *args):
        if len(args) == 1 and isinstance(args[0], Vertex) or isinstance(args[0], int):
            if isinstance(args[0], Vertex):
                value = args[0].get_value()
            elif isinstance(args[0], int):
                value = args[0]
            if self.find(vertex) > -1:
                for i, edge in enumerate(self.__edges):
                    if edge.get_beg().get_value() == value or edge.get_end().get_value() == value:
                        self.__edges.remove(self.__edges[i])
                vertex = self.find(value)
                self.__vertecies.remove(vertex)
            else:
                return False

        elif len(args) == 1 and isinstance(args[0], Edge):
            e = args[0]
            for i, edge in enumerate(self.__edges):
                if edge.get_beg().get_value() == e.get_beg().get_value() and edge.get_end().get_value() == e.get_end().get_value():
                    self.__edges.remove(self.__edges[i])

        elif len(args) == 2:
            beg = args[0]
            end = args[1]
            for i, edge in enumerate(self.__edges):
                if edge.get_beg().get_value() == beg and edge.get_end().get_value() == end:
                    self.__edges.remove(self.__edges[i])

    def get_vertex_dict(self):
        indexes = {}
        for i, vertex in enumerate(self.__vertecies):
            indexes[vertex.get_value()] = i
        return indexes

    def get_matrix(self):
        indexes = self.get_vertex_dict()
        size = range(self.get_vertecies_amount());
        matrix = [[0 for v in size] for v in size];
        for i, vertex in enumerate(self.__vertecies):
            for edge in self.get_edges():
                beg = edge.get_beg().get_value()
                end = edge.get_end().get_value()
                value = vertex.get_value()
                if value == beg or value == end:
                    matrix[indexes[beg]][indexes[end]] = 1
                    matrix[indexes[end]][indexes[beg]] = 1

        return matrix

    def children(self, vertex):
        children = []
        for edge in self.__edges:
            beg = edge.get_beg()
            end = edge.get_end()
            if beg.get_value() == vertex.get_value():
               children.append(end)
            if end.get_value() == vertex.get_value():
                children.append(beg)
        return children

    def unvisited_children(self, vertex, visited):
        children = self.children(vertex)
        unvisited = []
        for child in children:
            if not visited[child.get_value()]:
                unvisited.append(child)
        return unvisited

    def bfs(self, start, goal):
        lens = {}
        parents = {}
        visited = {}
        for v in self.__vertecies:
            id = v.get_value()
            visited[id] = False
            lens[id] = 0

        q = [start]
        visited[start] = True

        while (q):
            node = q.pop(0)
            if node == goal:
                length = lens[node]
                v = node
                path = []
                while(v in parents):
                    path.append(v)
                    v = parents[v]
                path.append(start)
                return (True, {'length': length, 'path': path[::-1]})
            vertex = self.get_vertex(node)

            unvisited = self.unvisited_children(vertex, visited)
            for child in unvisited:
                id = child.get_value()
                q.append(id)
                visited[id] = True
                lens[id] = lens[node] + 1
                parents[id] = node
        return False    

    def bfs_lite(self, start, goal):
        lens = {}
        visited = {}
        for v in self.__vertecies:
            id = v.get_value()
            visited[id] = False
            lens[id] = 0

        q = [start]
        visited[start] = True

        while (q):
            node = q.pop(0)
            if node == goal:
                length = lens[node]
                return length
            vertex = self.get_vertex(node)

            unvisited = self.unvisited_children(vertex, visited)
            for child in unvisited:
                id = child.get_value()
                q.append(id)
                visited[id] = True
                lens[id] = lens[node] + 1
        return False

    def dfs(self, start):
        visited, stack = set(), [start]
        edges = self.get_edges()
        while stack:
            vertex = stack.pop()
            if vertex not in visited:
                visited.add(vertex)
                for edge in edges:
                    if edge.get_beg().get_value() == vertex:
                        stack.append(edge.get_end().get_value())
        return visited
        
    def connected_components(self):
        seen = set()
        for v in self.__vertecies:
            if v.get_value() not in seen:
                c = set(self.dfs(v.get_value()))
                yield c
                seen.update(c)

    def get_number_components(self):
        return len(list(self.connected_components()))

    def union(self, graph):
        res = self
        for vert in graph.get_vertecies():
            id = vert.get_value()
            if not self.get_vertex(id):
                res.insert_vertex(vert.get_value())

        for e1 in graph.get_edges():
            exist = False
            for e2 in res.get_edges():
                if e2 == e1:
                    exist = True
            if not exist:
                res.insert_edge(e2)
        return res

    def intersection(self, graph):
        res = Graph()
        for e1 in self.get_edges():
            for e2 in graph.get_edges():
                if e2 == e1:
                    beg = e1.get_beg().get_value()
                    end = e1.get_end().get_value()
                    if not res.get_vertex(beg):
                        res.insert_vertex(beg)
                    if not res.get_vertex(end):
                        res.insert_vertex(end)
                    res.insert_edge(e1)
        return res


    # def find_cliques(potential_clique=[], remaining_nodes=[], skip_nodes=[], depth=0):

    # # To understand the flow better, uncomment this:
    # # print (' ' * depth), 'potential_clique:', potential_clique, 'remaining_nodes:', remaining_nodes, 'skip_nodes:', skip_nodes

    # if len(remaining_nodes) == 0 and len(skip_nodes) == 0:
    #     print 'This is a clique:', potential_clique
    #     return

    # for node in remaining_nodes:

    #     # Try adding the node to the current potential_clique to see if we can make it work.
    #     new_potential_clique = potential_clique + [node]
    #     new_remaining_nodes = [n for n in remaining_nodes if n in node.neighbors]
    #     new_skip_list = [n for n in skip_nodes if n in node.neighbors]
    #     find_cliques(new_potential_clique, new_remaining_nodes, new_skip_list, depth + 1)

    #     # We're done considering this node.  If there was a way to form a clique with it, we
    #     # already discovered its maximal clique in the recursive call above.  So, go ahead
    #     # and remove it from the list of remaining nodes and add it to the skip list.
    #     remaining_nodes.remove(node)
    #     skip_nodes.append(node)

    # find_cliques(remaining_nodes=all_nodes)

    # def find_cliques(self):
    #     if len(self.get_vertecies()) == 0:
    #             return iter([])
    #     adj = {u.get_value(): {v for v in u.get_dist() if v != 0} for u in self.get_vertecies()}
    #     Q = []
    #     vertecies = [v.get_value() for v in self.get_vertecies()]


    #     def expand(subg, cand):
    #         u = max(subg, key=lambda u: len(cand & adj[u]))
    #         for q in cand - adj[u]:
    #             cand.remove(q)
    #             Q.append(q)
    #             adj_q = adj[q]
    #             subg_q = subg & adj_q
    #             if not subg_q:
    #                 yield Q[:]
    #             else:
    #                 cand_q = cand & adj_q
    #                 if cand_q:
    #                     for clique in expand(subg_q, cand_q):
    #                         yield clique
    #             Q.pop()

    #     return expand(set(vertecies), set(vertecies))  

    # def cliques(self):
    #     r, x = [], []
    #     p = [v.get_value() for v in self.get_vertecies()]
    #     def bron_kerbosch(r, p, x):
    #         if not p and not x:
    #             return r
    #         for v in p:
    #             bron_kerbosch(r.union(v))
