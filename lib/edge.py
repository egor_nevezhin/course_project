# coding: utf-8
from .vertex import Vertex

class Edge:
    def __init__(self, beg, end):
        if isinstance(beg, Vertex) and isinstance(end, Vertex):
            self.__beg = beg
            self.__end = end
        else:
            self.__beg = Vertex(beg)
            self.__end = Vertex(end)

    def __eq__(self, other):
        return self.get_beg() == other.get_beg() and self.get_end() == other.get_end()


    def get_beg(self):
        return self.__beg

    def get_end(self):
        return self.__end

    def set_beg(beg):
        self.beg = beg

    def set_end(end):
        self.end = end;
