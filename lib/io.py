# coding: utf-8
from .vertex import Vertex
from .graph import Graph

def parse_vk_file(filename):
    file = open(filename, 'r')
    res = {'nodes': [], 'edges': []}
    for i, line in enumerate(file):
        line = line.replace('"', '').replace('\n', '')
        parts = line.split(',')

        if (i == 0):
            tmpl = parts

        elif (len(parts) > 2):
            t = {}
            for i, coll in enumerate(tmpl):
                t[coll] = parts[i]
            res['nodes'].append(t)

        elif (len(parts) == 1 and not line == 'V1 V2'):
            new_parts = tuple(line.split(' '))
            res['edges'].append(new_parts)

    return res

def from_vk_file_to_nx(filename):
    data = parse_vk_file(filename)
    import networkx as nx
    graph = nx.Graph()
    for node in data['nodes']:
        id = int(node['id'])
        graph.add_node(id, node)

    for edge in data['edges']:
        graph.add_edge(int(edge[0]), int(edge[1]))

    return graph

def from_list_to_nx(nodes, filename):
    data = parse_vk_file("vk/" + filename + ".txt")
    import networkx as nx
    graph = nx.Graph()

    for node in data['nodes']:
        if(int(node['id']) in nodes):
            id = int(node['id'])
            graph.add_node(id, node)

    for n1 in nodes:
        for n2 in nodes:
            if(n1 != n2):
                graph.add_edge(int(n1), int(n2))

    return graph

def from_vk_to_gviz(filename):
    file = open(filename, 'r')
    import networkx as nx
    graph = nx.Graph()
    res = []
    for i, line in enumerate(file):
        line = line.replace('"', '').replace('\n', '')
        parts = line.split(',')

        if (i == 0):
            tmpl = parts

        elif (len(parts) > 2):
            t = {}
            for i, coll in enumerate(tmpl):
                t[coll] = parts[i]
            #res['nodes'].append(t)
            id = int(t['id'])
            graph.add_node(id, t)

        elif (len(parts) == 1 and not line == 'V1 V2'):
            new_parts = line.split(' ')
            res.append((int(new_parts[0]), int(new_parts[1])))

    return res

def from_vk_file_to_graph(filename):
    data = parse_vk_file(filename)
    graph = Graph()
    for node in data['nodes']:
        id = int(node['id'])
        vert = Vertex(id, node)
        graph.insert_vertex(vert)

    for edge in data['edges']:
        graph.insert_edge(int(edge[0]), int(edge[1]))

    diameter = 0
    for v1 in graph.get_vertecies():
        maximum = 0
        for v2 in graph.get_vertecies():
            if not v1 == v2:
                if not v1.get_dist():
                    length = graph.bfs_lite(v1.get_value(), v2.get_value())
                    v1.set_dist(v2.get_value(), length)
                    v2.set_dist(v1.get_value(), length)
                    if length > maximum:
                        maximum = length
        v1.set_data('ec', maximum)
        if maximum > diameter:
            diameter = maximum
    graph.set_diameter(diameter)
    return graph

def from_vk_file_to_adj(filename):
    data = parse_vk_file(filename)
    adj = {}
    for node in data['nodes']:
        id = int(node['id'])
        if not id in adj:
            adj[id] = {}
        for edge in data['edges']:
            beg = int(edge[0])
            if id == beg:
                end = int(edge[1])
                adj[id][end] = None
    return adj

def from_vk_file_to_gt(filename):
    import graph_tool.all as gt
    data = parse_vk_file(filename)
    graph = gt.Graph(directed=False)
    for node in data['nodes']:
        id = int(node['id'])
        vert = graph.add_vertex(id)

    for edge in data['edges']:
        graph.add_edge(int(edge[0]), int(edge[1]))

    return graph

def save_to_bin(graph, filename):
    import pickle
    with open(filename, 'wb') as f:
        pickle.dump(graph, f, pickle.HIGHEST_PROTOCOL)
        f.close()

def read_from_bin(filename):
    import pickle
    with open(filename, 'rb') as f:
        try:
            graph = pickle.load(f)
        except EOFError:
            graph = False
    return graph

def get_graph(filename):
    import os.path
    vk_file_path = 'vk/' + filename + '.txt'
    bin_file_path = 'bin/' + filename + '.graph'
    graph = None
    if os.path.exists(bin_file_path):
        graph = read_from_bin(bin_file_path)
    if not graph:
        graph = from_vk_file_to_graph(vk_file_path)
        save_to_bin(graph, bin_file_path)
    return graph
