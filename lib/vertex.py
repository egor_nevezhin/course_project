# coding: utf-8

class Vertex:
    def set_value(self, value):
        self.__value = value

    def __init__(self, value, data = None):
        self.__value = value
        self.__dist = {}
        if (data):
            self.__data = data

    def __eq__(self, other):
        return self.get_value() == other.get_value()

    def __str__(self):
        return "({})".format(self.get_value())

    def get_value(self):
        return self.__value

    def get_data(self, key = None):
        if key:
            return self.__data[key]
        else:
            return self.__data

    def set_data(self, key, data = None):
        if data:
            self.__data[key] = data 
        else:
            data = key
            self.__data = data

    def set_dist(self, vert, dist):
        self.__dist[vert] = dist

    def get_dist(self, key = None):
        if key:
            if key in self.__dist:
                return self.__dist[key]
            else:
                return 0
        else:
            return self.__dist
