# coding: utf-8
from lib.graph import Graph
from lib.io import from_vk_file_to_graph, from_vk_file_to_nx, get_graph, from_list_to_nx
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

kirys = get_graph('3240_kirys')
def menu():
    from os import listdir
    from os.path import isfile, join
    mypath = 'vk/'
    files = [f[0:-4] for f in listdir(mypath) if isfile(join(mypath, f)) and f.find('.DS_S') == -1]
    options = [
        ['Матрица смежности', matrix],
        ['Визуализировать граф', draw],
        ['Показать распределение степеней связи', distribution],
        ['Кратчайщий путь для каждой пары вершин', shortest_paths],
        ['Диаметр графа', diameter],
        ['Объединение графов', union],
        ['Пересечение графов', intersection],
        ['Количество и компонеты графа', components],
        ['Клики', cliques]
    ]
    choice = -2
    while choice != -1:     
        print('Привет! Это курсовая. Выберите пункт:')        
        for i, option in enumerate(options):
            print('{}. {}'.format(i + 1, option[0]))
        choice = int(input()) - 1
        if choice == 5 or choice == 6:
            print('Выберите первый граф')
            for i, file in enumerate(files):
                print('{}. {}\n'.format(i + 1, file))
            choosen_file_1 = int(input()) - 1
            print('Выберите второй граф')
            for i, file in enumerate(files):
                print('{}. {}\n'.format(i + 1, file))
            choosen_file_2 = int(input()) - 1
            g1 = get_graph(files[choosen_file_1])
            g2 = get_graph(files[choosen_file_2])
            options[choice][1](g1, g2)
        else:
            print('Выберите граф')
            for i, file in enumerate(files):
                print('{}. {}\n'.format(i + 1, file))
            choosen_file = int(input()) - 1
            if choice == 1:
                options[choice][1](files[choosen_file])
            elif choice == 8:
                options[choice][1](files[choosen_file])
            else:
                g = get_graph(files[choosen_file])
                options[choice][1](g)


# Our graph instance
# name = 'test'
# g = get_graph('test')
name = '3240_egor_nevezhin'
g = get_graph(name)

## 1: Получить матрицу
def matrix(g):
    matrix = g.get_matrix()
    print('1. Матрица смежности: \n')
    for row in matrix:
        print(row)


## 2: Визуализировать граф
def getLabels(graph):
    res = {}
    indexes = graph.get_vertex_dict()
    for vertex in graph.get_vertecies():
        index = indexes[vertex.get_value()]
        res[index] = str(vertex.get_value())
    return res

def getNames(graph):
    res = {}
    for vertex in graph.get_vertecies():
        id = vertex.get_value()
        name = vertex.get_data('first_name') + ' ' + vertex.get_data('last_name')
        res[id] = name
    return res

def getNamesNX(graph):
    res = {}
    for vertex in graph.nodes(data=True):
        id = vertex[0]
        name = vertex[1]['first_name'] + ' ' + vertex[1]['last_name']
        res[id] = name
    return res

def draw(name):
    graph_nx = from_vk_file_to_nx('vk/{}.txt'.format(name))
    pos = nx.spring_layout(graph_nx, iterations=5)
    nx.draw(graph_nx, pos) 
    nx.draw_networkx(graph_nx, pos, ax = None, hold=None, labels=getNamesNX(graph_nx), font_color="blue", node_size = 50)
    plt.show()

## 3 — 4: Диаграмма количества общих друзей
def drawDistribution(graph):
    distr = graph.get_degree_distribution()
    plt.bar(distr.keys(), distr.values(), 1, color='g')
    plt.show()

def distribution(g):
    drawDistribution(g)

# drawDistribution(g) # Раскомментить

## 5: Для каждой пары вершин найти кратчайший путь
## Находится в from_vk_file_to_graph
def shortest_paths(g):
    print('5. Кратчайщий путь для каждой пары вершин')
    for vert1 in g.get_vertecies():
        for vert2 in g.get_vertecies():
            if not vert1 == vert2 and vert1.get_dist(vert2.get_value())> 0:
                print('{} — {}: {}'.format(vert1, vert2, vert1.get_dist(vert2.get_value())))

## 6: Найти диаметр графа
def diameter(g):
    print('6. Диаметр графа = {}'.format(g.get_diameter()))  

## 7: Пересечение и объединение
def union(g1, g2):
    print(g1)
    print(g2)
    print(g1.union(g2))

def intersection(g1, g2):
    print(g1)
    print(g2)
    print(g1.intersection(g2))

## 8: Найти количество компонент графа
def components(g):
    print('8. Количество компонент графа = {}'.format(g.get_number_components()))

def cliques(name):
    graph_nx = from_vk_file_to_nx('vk/{}.txt'.format(name))
    cl = list(nx.find_cliques(graph_nx))
    print(len(cl))
    for clique in reversed(cl):
        gr = from_list_to_nx(clique, name)
        pos = nx.spring_layout(gr, iterations=5)
        nx.draw(gr, pos) 
        nx.draw_networkx(gr, pos, ax = None, hold=None, labels=getNamesNX(gr), font_color="blue", node_size = 50)
        plt.show()



## 9: Алгоритм поиска кластеров
# print(list(g.find_cliques()))

menu()
## Чтото из NetworkX
# pos = nx.spring_layout(egor_nevezhin, k=0.1, scale=10000, iterations=15)
# nx.draw(egor_nevezhin, pos) 
# nx.draw_networkx_labels(egor_nevezhin, pos, getNamesNX(egor_nevezhin), font_color="blue")
# plt.show()